# Outdoor Survival

[[_TOC_]]

This is my remake of the board game _Outdoor Survival_.  The original game
was published by Avalon Hill Game Company in 1972. 

## Game mechanics 

|              |                    |
|--------------|--------------------|
| Period       | Modern             |
| Level        | Strategic          |
| Hex scale    | 6 km (3.7 miles)   |
| Unit scale   | Person             |
| Turn scale   | 1 day              |
| Unit density | Low                |
| # of turns   | Variable           |
| Complexity   | 1 of 10            |
| Solitaire    | 9 of 10            |

Features:
- Scenarios
- Randomised movement

## About this rework 

This rework is entirely new.  All text and graphics is new and nothing
is copied verbatim from the original materials. 

I have restructured and rewritten the rules somewhat (the text is all
new), as well as added illustrations of the rules.

## The board 

The board is quite big (65.6cm wide and 59.5cm tall - or 25.8" by
23.4"), and does not fit into readily available printers.  The board
is therefore presented as a 4-part document.  There are two version:

- One designed to be printed on A3 paper ([splitboardA3.pdf][]).  This
  holds a relatively comfortable size of counters.
  
- One designed to be printed on A4 paper ([splitboardA4.pdf][]).  This
  is further split into to two, resulting in 8 parts. 
  
(for US Tabloid and Letter, replace `A3` with `Tabloid` and `A4` with
`Letter`). 
  
Print your PDF of choice onto 4 or 8 separate sheets of paper (if you
have a duplex printer, simply print the document) and glue on to
sturdy cardboard (I like 1 1/2mm poster cardboard).  Cut along dashed
crop lines.  You can hold the 4 or 8 pieces together with paper clamps
or the like.

Alternatively you can glue the 4 or 8 sheets of paper together.  Cut
one end along the crop marks (dashed lines) and glue on to the next
part, taking care to align the crop mark arrows.  When playing the
game, put a transparent heavy plastic sheet on top to hold the board
down.

Remember to use the appropriate `materialsX.pdf` file for your board. 

| Board<br>Paper | Board<br>file | Materials<br>file | Materials<br>paper  |
|---------|---------------------------|--------------------------|--------|
| A3      | [splitboardA3.pdf][]      | [materialsA4.pdf][]      | A4     |
| A4      | [splitboardA4.pdf][]      | [materialsA4.pdf][]      | A4     |
| Tabloid | [splitboardTabloid.pdf][] | [materialsLetter.pdf][]  | Letter |
| Letter  | [splitboardLetter.pdf][]  | [materialsLetter.pdf][]  | Letter |

If an A3 print is not available, and the 8 part board is unmanageable,
then one can scale down [splitboardA3.pdf][] by a factor of
$`1/\sqrt{2}=0.7071`$ to get a document that can be printed on an A4
printer.  In that case, one should _also_ scale down
[materialsA4.pdf][] by the same factor and use the counters and OOB
form that scaled down copy.

To scale down for Letter paper, one should apply a scale factor of
$`0.65`$ to both [splitboardTabloid.pdf][] and
[materialsLetter.pdf][]. 

## The files 

The distribution consists of the following files 

- [OutdoorSurvivalA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and order of battle, and
  the board split into 8 parts.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.
  
  Note that the PDF is a little heavy to load (due to the large map),
  so a bit of patience is needed. 
  
- [OutdoorSurvivalA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 4 sheets which should be folded
  down the middle of the long edge, and stabled to form an 16-page A5
  booklet of the rules.
  
- [splitboardA3.pdf][] holds the board in 6 sheets of A3 paper.  Print
  these and glue on to a sturdy piece of cardboard.  You can perhaps
  make some clever arrangement that allows you to unfold the map.
  Otherwise, use paper clambs or the like to hold the board pieces
  together.  
  
- [splitboardA4.pdf][] is like [splitboardA3.pdf][] above, but further
  split to fit on 8 sheets of A4 paper. 
  
- [materialsA4.pdf][] holds the charts, counters, and OOB.  It is
  meant to be printed on A4 paper.  Print and glue on to   a
  relatively thick piece of cardboard (1.5mm or so) or the like. 
  
- [board.pdf][] Is the whole board as a PDF.  If you have a way of
  printing such a large file, you may find this useful. 

If you only have access to US Letter (and possibly Tabloid) printer,
you can use the following files

| *A4 Series*                      | *Letter Series*                       |
| ---------------------------------|---------------------------------------|
| [OutdoorSurvivalA4.pdf][]        | [OutdoorSurvivalLetter.pdf][]  	   |
| [OutdoorSurvivalA4Booklet.pdf][] | [OutdoorSurvivalLetterBooklet.pdf][]  |
| [materialsA4.pdf][]	           | [materialsLetter.pdf][]	           |
| [splitboardA4.pdf][]	           | [splitboardLetter.pdf][]	           |
| [splitboardA3.pdf][]	           | [splitboardTabloid.pdf][]	           |

Note, the US letter files are scaled down to 95% relative to the A4
files.  This is to fit everything in on the same scale.  This should
not be a problem for most users as most of the world is using A4
rather than the rather obscure Letter format. 

Download [artifacts.zip][] to get all files for both kinds of paper
formats. 

## VASSAL module 

A [VASSAL](https://vassalengine.org) module of the game is available

- [OutdoorSurvival.vmod][] Regular version 

The modules are the same, except for the graphics used.  The modules
implements many automatic features and all the scenarios.

## Previews 

[![The board](.imgs/board.png)](https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-A4-master/splitboardA3.pdf?job=dist "The board")
[![The rules](.imgs/front.png)](https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-A4-master/OutdoorSurvivalA4.pdf?job=dist)
[![The charts](.imgs/charts.png)](https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-A4-master/materialsA4.pdf?job=dist)
[![The counters](.imgs/counters.png)](https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-A4-master/materialsA4.pdf?job=dist)

## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This package,
combined with the power of LaTeX, produces high-quality documents,
with vector graphics to ensure that everything scales.   Since the
board is quite big, we must use Lua$`\mathrm{\LaTeX}`$ for the
formatting. 


## The General articles 

[All _The General_
issues](https://www.vftt.co.uk/ah_mags.asp?ProdID=PDF_Gen) as scanned PDFs.


[artifacts.zip]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/download?job=dist

[OutdoorSurvivalA4.pdf]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-A4-master/OutdoorSurvivalA4.pdf?job=dist
[OutdoorSurvivalA4Booklet.pdf]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-A4-master/OutdoorSurvivalA4Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-A4-master/materialsA4.pdf?job=dist
[splitboardA3.pdf]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-A4-master/splitboardA3.pdf?job=dist
[splitboardA4.pdf]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-A4-master/splitboardA4.pdf?job=dist
[board.pdf]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-A4-master/board.pdf?job=dist
[OutdoorSurvival.vmod]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-A4-master/OutdoorSurvival.vmod?job=dist

[OutdoorSurvivalLetter.pdf]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-Letter-master/OutdoorSurvivalLetter.pdf?job=dist
[OutdoorSurvivalLetterBooklet.pdf]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-Letter-master/OutdoorSurvivalLetterBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-Letter-master/materialsLetter.pdf?job=dist
[splitboardTabloid.pdf]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-Letter-master/splitboardTabloid.pdf?job=dist
[splitboardLetter.pdf]: https://gitlab.com/wargames_tex/outdoor_tex/-/jobs/artifacts/master/file/OutdoorSurvival-Letter-master/splitboardLetter.pdf?job=dist






