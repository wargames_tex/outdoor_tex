# TODO
# 
# - Place only as many search counters as given by active persons and
#   the scenarios.
# - Clean up
# - Tutorial?
#
# Other ideas
# - Module could increment sleep if person not moved
#   (requires auto-clear of move mark)
# - Module could delete person if expired 
# 
# from wgexport import *
from pprint import pprint
from __main__ import *

doTutorial = True # Change this to false for final 

# --------------------------------------------------------------------
class DummyElement(Element):
    def __init__(self,parent,node=None,**kwargs):
        super(DummyElement,self).__init__(parent,'Dummy',node=node)
        
# --------------------------------------------------------------------
moreHelp = '''
<html>
 <head>
  <title>{title}</title>
  <style>
  </style>
 </head>
 <body>
  <h1>{title}</h1>

  <center>
    <img src="Splash.png" width=200 height=277>
  </center>

  <h2>Rules</h2>
  <p>
    A PDF of the rules is available through the <b>Help</b> menu.
    Please note that it may take a little while for your PDF viewer
    to launch
  </p>
  <h2>How to use</h2>

  <p> To ensure proper execution of the module and game, please use
    the turn tracker (<code>Alt-T</code> or &quot;<b>+</b>&quot;) to
    move forward) judiciously.</p>

  <p>
    Upon starting a new game, you will be presented with a <b>Set-up</b>
    window.   Here, you can
  </p>

  <ul>

    <li>turn on or off persons by clicking the boxes (<img
      src="opt-icon.png" width=18 height=16>) to the left of the
      person counters,</li>

    <li>turn on or off optional rules by clicking the boxes (<img
      src="opt-icon.png" width=18 height=16>) to the left of the
      optional rule names, and</li>

    <li>select the scenario to play by clicking the scenario piece
      <img src="scenario_basic.png" width=24 height=24>.</li>
  </ul>

  <p>
    Once you have adjusted the settings to your liking, you can close
    the window if you like (you can always bring it back by pressing
    <code>Alt-Q</code>, but you cannot change settings later on).
  </p>

  <p> The optional rules <b>Encounter</b> and <b>Random type</b> are
    fully implemented in the module.  The remaining two
    <b>Cottages</b> and <b>Limited life</b> are not, are merely here
    to enable tracking of the optional rules used.</p>

  <p>
    Now you should press <code>Alt-T</code> to move on to the next
    phase of the game. At this point (&quot;Setup&quot; phase),
  </p>

  <ul>

    <li>the enabled person counters (<img src="person.png" width=24
      height=24>) will be moved to their starting hexes, </li>

    <li>possible lost persons and decoy markers will be placed at
      random on the map, and</li>

    <li>the optional rules will be updated for the chosen scenario.</li>
  </ul>

  <p>
    Note, some scenarios, such as the <b>Pursue</b> scenario allows
    players to choose between starting position.  In those cases, you
    should allow players to adjust their starting hex - within the
    bounds of the rules, of course.  
  </p>

  <p> Also, if the scenario, again for example <b>Pursue</b>, calls
   for placement of outposts (<img src="outpost_1.png" width=24
   height=24>), then those should be placed on the map at this point.
   </p>

  <p> If the scenario uses lost person markers (<img src="mia_1.png"
    width=24 height=24>) and decoys (<img src="decoy_1.png" width=24
    height=24>), then those will have been placed (at random)
    automatically.  However, if you and your follow players are not
    happy with the placements, for example because more than one
    marker is in the same hex, you can under mutual agreement move
    them around at this point. <i>Do not</i>, however, flip them over
    as it will ruin the surprise and game.</p>

  <p> Once these adjustments and other set-ups are done, press
    <code>Alt-T</code> to begin the game proper.  Each press of
    <code>Alt-T</code> (or alternatively the &quot;<b>+</b>&qout;
    button on the turn tracker) will take game to the next phase, and
    some automatic actions will be taken.  </p>

  <p>
    The game phases are, for each enable person,
  </p>
  <ol>
    <li>Movement</li>
    <li>Necessities</li>
    <li>Encounter (skipped if not enabled)</li>
  </ol>

 <h3>Movement</h3>

 <p>
   When moving to this phase, then the current persons sleep counter is
   automatically moved up one slot.</p>

 <p>If a person decides <i>not</i> to move this turn, or is prevent
   from moving, then that person should adjust the sleep (<img
   src="sleep-icon.png" width=25 height=16>) level on the levels chart
   (<code>Alt-B</code>) and then simply hit <code>Alt-T</code> to move
   on to the next phase.</p>

  <p>If a person can move and decides to do so, the the person should
    hit <code>Ctrl-A</code> (or <img src="abb-icon.png" width=21
    height=16>) to determine the current turns movement abilities.
    The result is printed in the chat in the form of <i>X/Y/Z</i> and
    is further explain in the rules.  </p>

  <p>If the result calls for a random direction (<i>X=R</i>), then
   that direction is automatically determined by a die roll.
   Alternatively, use <code>Ctrl-R</code> (or <img src="dir-icon.png"
   width=14 height=16>) to resolve this.  The direction is printed in
   the chat (<code>N</code> for north, <code>NE</code> for north east,
   <code>SE</code> for south east, and so on).</p>

  <p>Once the person has implemented its moves, it should press
    <code>Alt-T</code> to move on to the next phase.</p>

  <h3>Necessities</h3>

  <p>
   Necessities are <i>not</i> resolved automatically by the module.
  </p>

  <center>
    <img src="B_levels.png" width=200 height=196>
  </center>

  <p>Necessities are tracked in the persons level chart available via
   <code>Alt-B</code> (or <img src="oob-icon.png" width=15
   height=16>). See above for an example.  </p>

  <p> This means that in this phase, a person <i>must</i> must adjust
   its <b>Food</b> (<img src="food-icon.png" width=10 height=16>) and
   <b>Water</b> (<img src="water-icon.png" width=23 height=16>) levels
   by hand according to the rules of the game and the scenario
   requirements (press <code>Alt-A</code> to get an overview of
   necessity requirements).</p>

  <p><b>Do not</b> adjust <b>life</b> (<img src="life-icon.png"
    width=16 height=16>) levels by hand.  This level is automatically
    updated by the module.</p>

  <p>If your life level falls below &quot;O&quot; then you have not
    survived and you lose the game.</p>

  <p>After adjusting the necessities, press <code>Alt-T</code> to move
   on.  </p>

  <h3>Encounter (optional)</h3>

  <p>
   If the <i>Encounter</i> optional rule is not in effect, then this
   phase is skipped automatically by the module, and will move on to
   the next persons turn.  </p>

  <p>If, in addition to the <i>Encounter</i>, the <i>Random type</i>
   optional rule is in effect, then the module will automatically</p>

  <ol>
   <li>Roll to see if there is an encounter,</li>
   <li>Roll for the encounter type, and</li>
   <li>Roll and implement the encounter result.</li>
  </ol>

  <p>(All three die rolls will be made, irrespective of the outcomes).</p>

  <p> That is, if <i>Random type</i> is enabled, then the encounter is
  automatically resolved <i>and</i> implemented for you.</p>

  <p>If the <i>Random type</i> option is <i>not</i> implemented,
    then you <i>must</i> select the encounter type yourself, either via
    the context menu, or by pressing
  </p>
  <ul>

   <li><code>Ctrl-N</code> (<img src="nat-icon.png" width=21
     height=16>) for a natural hazard,</li>

   <li><code>Ctrl-I</code> (<img src="elk-icon.png" width=21
     height=16>) for an animal or insect encounter, or</li>

   <li><code>Ctrl-P</code> (<img src="per-icon.png" width=12
   height=16>) for a personal event.</li>

  </ul>

  <p>The module will then</p>

  <ol>
   <li>Roll to see if there is an encounter,</li>
   <li>Roll and implement the encounter result.</li>
  </ol>

  <p>(Both die rolls will be made, irrespective of the first outcome).</p>

  <p>The module will give you a kind reminder.</p>

  <p>Once done with the encounter, press <code>Alt-T</code> to move to
    next persons turn.</p>

  <h3>End of game</h3>

  <p>How the game ends depends on the scenario.  Please refer to the
    rules and the scenario description there for more information.  </p>

  <h2>Chat message</h2>

  <p>Generally, messages that call for the user to do some actions are
  shown in purple text in the chat log. Reminders are in an italic
  type-face.</p>

  <p>Messages that inform the user of results of die rolls or the like
  is shown in green text.</p>

  <p>Phase and turn changes are shown in bold text</p>

  <h3>Verbosity and debug message</h3>

  <p>The module can be made verbose (default) and also print out
   debugging message (off by default) via the <b>Preferences</b>
   file-menu item (select the <i>{title}</i> tab).
  </p>

  <h2>Credits</h2>

  <dl>
   <dt>Design:</dt>
   <dd>Jim Dunnigan</dd>
  </dl>

  <h2>Copyright and license</h2>

  <p> This work is &#127279; 2023 Christian Holm Christensen, and
   licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit </p>

  <center>
   <code>http://creativecommons.org/licenses/by-sa/4.0</code>
  </center>

  <p>
    or send a letter to
  </p>

  <center>
   Creative Commons<br>
   PO Box 1866<br>
   Mountain View<br>
   CA 94042<br>
   USA
  </center>
</body>
</html>'''

# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,gverbose=False):
    from re  import sub
    from PIL import Image
    from io  import BytesIO

    Verbose().setVerbose(gverbose)
    verb = VerboseGuard()
    
    game = build.getGame()

    # ================================================================
    #
    # Keys, global properties
    #
    # ----------------------------------------------------------------
    verb(f'Defining keys and variable names')
    keyToStart     = key(NONE,0)+',toStart'
    keyAbilities   = key('A')
    keyRandom      = key('R')
    keyEncounter   = key('E')
    keyNatural     = key('N')
    keyAnimal      = key('I')
    keyPersonal    = key('P')
    keyIncr        = key('[', ALT)
    keyDecr        = key(']', ALT)
    keyRandomTramp = key(NONE,0)+',keyRandomTrampoline'
    keyIncrVal     = key(NONE,0)+',keyIncrVal'
    keyDecrVal     = key(NONE,0)+',keyDecrVal'
    keyIncrReal    = key(NONE,0)+',keyIncrReal'
    keyDecrReal    = key(NONE,0)+',keyDecrReal'
    keyIncrLife    = key(NONE,0)+',keyIncrLife'
    keyDecrLife    = key(NONE,0)+',keyDecrLife'
    keyDecrSleep   = key(NONE,0)+',keyDecrSleep'
    keyMove        = key(NONE,0)+',keyMove'
    keySet         = key(NONE,0)+',keySet'
    keySetLife     = key(NONE,0)+',keySetLife'
    keySetup       = key('Q',ALT)
    keyToggle      = key(NONE,0)+',keyToggle'
    keySetOpt      = key(NONE,0)+',keySetOpt'
    diceName       = '1d6'
    fourDiceName   = '4d6'
    keyDice        = key('6',ALT)
    keyFourDice    = key(NONE,0)+',keyFourDice'
    keyDieRoll     = key(NONE,0)+',keyDieRoll'
    keyRndTpe      = key(NONE,0)+',keyRndTpe'
    keyNatTpe      = key(NONE,0)+',keyNatTpe'
    keyAniTpe      = key(NONE,0)+',keyAniTpe'
    keyPerTpe      = key(NONE,0)+',keyPerTpe'
    keyEnc         = key(NONE,0)+',keyEnc'
    keyEncEnb      = key(NONE,0)+',keyEncEnb'
    keySlpIncr     = key(NONE,0)+',keySlpIncr'
    keyWtrIncr     = key(NONE,0)+',keyWtrIncr'
    keyWtrDecr     = key(NONE,0)+',keyWtrDecr'
    keyFodIncr     = key(NONE,0)+',keyFodIncr'
    keyFodDecr     = key(NONE,0)+',keyFodDecr'
    keyLifIncr     = key(NONE,0)+',keyLifIncr'
    keyLifDecr     = key(NONE,0)+',keyLifDecr'
    keyUpdate      = key(NONE,0)+',keyUpdate'
    keyClean       = key(NONE,0)+',keyClean'
    keyCalcAbility = key(NONE,0)+',keyCalcAbility'
    keyCalcRanDir  = key(NONE,0)+',keyCalcRanDir'
    
    globalScenario     = 'Scenario'
    globalCurrent      = 'CurrentPerson'
    globalCurrentLower = 'CurrentPersonLower'
    globalNotMovement  = 'NotMovement'
    globalNotSetup     = 'NotSetup'
    globalNotEncounter = 'NotEncounter'
    hidden             = 'os hidden'
    notSetup           = 'notSetup'
    keyNotSetup        = key(NONE,0)+',setNotSetup'
    debug              = 'wgDebug'
    verbose            = 'wgVerbose'
    optEncounter       = 'optEncounter'
    optRandom          = 'optRandomEncounter'
    optCottage         = 'optCottage'
    optLimited         = 'optLimited'

    # ================================================================
    #
    # Global properties
    #
    # ----------------------------------------------------------------
    verb(f'Defining global properties')
    gp                = game.getGlobalProperties()[0];
    gp.addProperty(name         = globalScenario,
                   initialValue = 0,
                   isNumeric    = True,
                   min          = 0,
                   max          = 5)
    gp.addProperty(name         = globalCurrent,
                   initialValue = '',
                   isNumeric    = False)
    gp.addProperty(name         = globalCurrentLower,
                   initialValue = '',
                   isNumeric    = False)
    gp.addProperty(name         = globalNotMovement,
                   initialValue = True,
                   isNumeric    = True)
    gp.addProperty(name         = globalNotSetup,
                   initialValue = False,
                   isNumeric    = True)
    gp.addProperty(name         = globalNotEncounter,
                   initialValue = True,
                   isNumeric    = True)
    for o,t in [[optEncounter, "Encounters"],
                [optRandom,    "Random encounter type"],
                [optCottage,   "Cottages as source"],
                [optLimited,   "Limited life"]]:
        gp.addProperty(name         = o,
                       initialValue = True,
                       description  = t)
    
    # ================================================================
    #
    # Default preferences
    #
    verb(f'Defining global options')
    go    = game.getGlobalOptions()[0]
    prefs = go.getPreferences()
    prefs[debug]          ['default'] = False
    prefs[verbose]        ['default'] = False
    # prefs['wgAutoOdds']   ['default'] = True
    # prefs['wgAutoResults']['default'] = True
            
    # ================================================================
    #
    # Inventory
    #
        
    # ================================================================
    #
    # Dice
    #
    game.addDiceButton(name       = fourDiceName,
                       hotkey     = keyFourDice,
                       icon       = '',
                       text       = '',
                       nDice      = 4)
    # ================================================================
    #
    # Piece window icon
    #
    game.getPieceWindows()['Counters']['icon'] = 'person.png'
    
    # ================================================================
    #
    # Get prototype container
    #
    prototypeContainer = game.getPrototypes()[0]

    # ================================================================
    #
    # Get maps and main map 
    # 
    verb(f'Get maps')
    maps          = game.getMaps()
    main          = maps['Board']
    main['color'] = rgb(255,215,79)

    
    # ================================================================
    #
    # Extra documentation
    #
    verb(f'Add extra documentation')
    doc  = game.getDocumentation()[0]
    doc.addHelpFile(title='More information',fileName='help/more.html')
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))

    # ================================================================
    #
    # Boards, etc.
    #
    # ----------------------------------------------------------------
    # Get the restore global key, set icon and remove the deadmap map,
    # since we will move eliminated units to the OOBs instead.
    verb(f'Removing dead pool')
    restore                 = maps['DeadMap'].getMassKeys()['Restore']
    oobs                    = game.getChartWindows()['OOBs']
    oobs['icon']            = 'oob-icon.png'
    game.remove(maps['DeadMap'])
    # main.append(restore)

    # ================================================================
    for side in 'ABCD':
        soob = maps[f'{side} levels']
        soob.addMassKey(name         = 'Decrement Value',
                        buttonHotkey = keyDecr,
                        hotkey       = keyDecr,
                        buttonText   = '-',
                        icon         = '/icons/32x32/go-next.png',
                        reportSingle = True,
                        reportFormat = '',
                        # target       = '', # Selected
                        # filter       = f'{{BasicName == "{shrt} moves"}}',
                        tooltip      = 'Decrement selected value')
        soob.addMassKey(name         = 'Increment Value',
                        buttonHotkey = keyIncr,
                        hotkey       = keyIncr,
                        buttonText   = '+',
                        icon         = '/icons/32x32/go-previous.png',
                        reportSingle = True,
                        reportFormat = '',
                        # target       = '', # selected 
                        # filter       = f'{{BasicName == "{shrt} moves"}}',
                        tooltip      = 'Increment selected value')
        
    # ================================================================
    #
    # Setup window
    #
    setp = maps['Setup']
    setp['allowMultiple'] = False
    setp['markMoved'] = 'Never'
    setp['hotkey']    = keySetup
    setp['launch']    = True
    setp['icon']      = 'opt-icon.png'
    setp.remove(setp.getImageSaver()[0])
    setp.remove(setp.getTextSaver()[0])
    setp.remove(setp.getGlobalMap()[0])
    setp.remove(setp.getHidePiecesButton()[0])
    obrd = setp.getBoardPicker()[0].getBoards()['Setup']
    okeys = setp.getMassKeys()
    # print(okeys)
    for kn,k in okeys.items():
        # print(f'Removing {kn} from Optionals')
        setp.remove(k) 
    ostart = setp.getAtStarts(False)
    for at in ostart:
        # print(at['name'])
        if at['name'] == hidden:
            # print(f'Removing {at["name"]} from Optionals')
            setp.remove(at)
    
    game.addStartupMassKey(name        = 'Setup',
                           hotkey      = keySetup,
                           target      = '',
                           filter      = f'{{BasicName=="{hidden}"}}',
                           whenToApply = StartupMassKey.START_GAME,
                           reportFormat=
                           f'{{{debug}?("~ Show optionals window"):""}}')
    
    # ----------------------------------------------------------------
    # Set custom icons on some global keys 
    verb(f'Set custom icons')
    mkeys = main.getMassKeys()
    main.remove(mkeys['Eliminate'])
    # main.remove(mkeys['Flip'])
    mkeys['Flip']     ['icon'] = 'flip-icon.png'

    # ----------------------------------------------------------------
    # Get Zoned area
    verb(f'Get zones and adjust grid')
    zoned = main.getBoardPicker()[0].getBoards()['Board'].getZonedGrids()[0]

    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zones  = zoned.getZones()
    hzone  = zones['hexes']
    hgrids = hzone.getHexGrids()
    hgrid  = hgrids[0]
    hnum   = hgrid.getNumbering()[0]
    # hnum['hOff']  = 0
    # print(f'Numbering off {hnum["hOff"]} and {hnum["vOff"]}')
    hnum['vOff'] = int(hnum['vOff'])-3
    hnum['sep']  = '@'

    # ================================================================
    #
    # Turn track
    #
    # ----------------------------------------------------------------
    verb(f'Adjusting turns')
    turns         = game.getTurnTracks()['Turn']
    phaseNames    = ['Start','Setup']
    personPhases  = ['Movement',
                     'Necessities',
                     'Encounter' ]
                     
    for side in 'ABCD':
        phaseNames.extend([f'{side} {phase}' for phase in
                           personPhases])
    phases         = turns.getLists()['Phase']
    phases['list'] = ','.join(phaseNames)
    turns['reportFormat'] = '--- <b><i>$newTurn$</i></b> ---'
    turns.addHotkey(name   = 'Clean up optionals',
                    hotkey = keyClean,
                    match  = (f'{{Turn==1&&Phase=="{phaseNames[1]}"}}'))
    turns.addHotkey(name   = 'Update on phase change',
                    hotkey = keyUpdate,
                    reportFormat = f'{{{debug}?("~ update on phase"):""}}')
    for skip in phaseNames[:2]:
        turns.addHotkey(hotkey       = key('T',ALT), # This works!
                        match        = f'{{Phase=="{skip}"&&Turn>1}}',
                        name         = 'Skip set-up phase after turn 1')
    for side in 'ABCD':
        for phase in personPhases:
            turns.addHotkey(
                hotkey       = key('T',ALT), # This works!
                match        = (f'{{Phase=="{side} {phase}"&&'
                                f'{side}_Enable==false}}'),
                name         = f'Skip {side} {phase} phase')
        turns.addHotkey(
            hotkey = key('T',ALT), # This works!
            match  = (f'{{Phase.contains("{personPhases[2]}")&&'
                      f'{optEncounter}==false}}'),
            name   = 'Skip encounter if disabled')
        turns.addHotkey(
            hotkey = key('T',ALT), # This works!
            match  = (f'{{Phase.contains("{personPhases[1]}")&&'
                      f'{globalScenario}==0}}'),
            name   = 'Skip necessities in basic')
    turns.addHotkey(
        name         = 'Move to start positions',
        hotkey       = keyToStart,
        match        = (f'{{Turn==1&&Phase=="{phaseNames[1]}"}}'),
        reportFormat = (f'{{{debug}?("Move to start hexes"):""}}'))
    turns.addHotkey(
        name         = 'Automatic encounter',
        hotkey       = keyEncounter,
        match        = (f'{{Phase.contains("{personPhases[2]}")&&'
                        f'{optEncounter}==true&&'
                        f'{optRandom}==true}}'),
        reportFormat = '! Encounter automatically resolved')
    turns.addHotkey(
        name         = 'Encounter reminder',
        hotkey       = key(NONE,0)+',nothing',
        match        = (f'{{Phase.contains("{personPhases[2]}")&&'
                        f'{optEncounter}==true&&'
                        f'{optRandom}==false}}'),
        reportFormat = '` <i>Remember to resolve encounters</i>')
    turns.addHotkey(
        name         = 'Necessities reminder',
        hotkey       = key(NONE,0)+',nothing',
        match        = (f'{{Phase.contains("{personPhases[1]}")}}'),
        reportFormat = '` <i>Remember to resolve necessities</i>')
    # turns.addHotkey(
    #     name         = 'Decrement sleep',
    #     hotkey       = keyDecrSleep,
    #     match        = (f'{{Phase.contains("{personPhases[0]}")}}'),
    #     reportFormat = (f'{{"` <i>Decrement sleep for "+'+
    #                     f'{globalCurrentLower}+" in "+'
    #                                 f'Phase+"</i>"}}')
    #                 )
                    

    # ----------------------------------------------------------------
    # Specific actions 
    # turns.addHotkey(hotkey       = flipTurn,
    #                 match        = f'{{Phase=="{phaseNames[germanNo]}"}}',
    #                 reportFormat = '--- <b>German Turn</b> ---',
    #                 name         = 'German Turn')

    # ================================================================
    #
    # Global key commands
    #
    main.addMassKey(name         = 'Place at start',
                    buttonHotkey = keyToStart,
                    hotkey       = keyToStart,
                    buttonText   = '',
                    canDisable   = True,
                    # propertyGate = '{Turn!=1||Phase!="Setup"}',
                    target       = '',
                    filter       = '{Type=="Person"||Type=="Search"}',
                    singleMap    = False,
                    reportFormat = (f'{{{verbose}?("` Placing persons at "+'
                                    f'"approximate starting positions for "+'
                                    f'"scenario # "+{globalScenario}+'
                                    f'". Please adjust according to the rules"'
                                    f'):""}}'))
    main.addMassKey(name         = 'Random direction',
                    buttonHotkey = keyRandom,
                    hotkey       = keyRandom,
                    buttonText   = '', # Replace with icon
                    icon         = 'dir-icon.png',
                    target       = '',
                    filter       = f'{{BasicName=={globalCurrentLower}+" a"}}',
                    canDisable   = True,
                    propertyGate = f'{globalNotMovement}',
                    reportFormat = (f'{{{debug}?("~ Random direction for "+'
                                    f'{globalCurrentLower}):""}}'))
    main.addMassKey(name         = 'Movement abilities',
                    buttonHotkey = keyAbilities,
                    hotkey       = keyAbilities,
                    buttonText   = '', # Replace with icon
                    icon         = 'abb-icon.png',
                    target       = '',
                    filter       = f'{{BasicName=={globalCurrentLower}+" a"}}',
                    canDisable   = True,
                    propertyGate = f'{globalNotMovement}',
                    reportFormat = (f'{{{debug}?("~ Movement abilities for "+'
                                    f'{globalCurrentLower}):""}}'))
    main.addMassKey(name         = 'Encounter',
                    buttonHotkey = keyEncounter,
                    hotkey       = keyEncounter,
                    buttonText   = '', # 'Encounter', # Replace with icon
                    icon         = '', # 'enc-icon.png',
                    target       = '',
                    filter       = f'{{BasicName=={globalCurrentLower}+" a"}}',
                    canDisable   = False,
                    propertyGate = f'{globalNotEncounter}',
                    reportFormat = (f'{{{debug}?("~ Encounter for "+'
                                    f'{globalCurrentLower}):""}}'))
    main.addMassKey(name         = 'Natural',
                    buttonHotkey = keyNatural,
                    hotkey       = keyNatural,
                    buttonText   = '', # 'Encounter', # Replace with icon
                    icon         = 'nat-icon.png',
                    target       = '',
                    filter       = f'{{BasicName=={globalCurrentLower}+" a"}}',
                    canDisable   = True,
                    propertyGate = f'{globalNotEncounter}',
                    reportFormat = (f'{{{debug}?("~ Natural hazard for "+'
                                    f'{globalCurrentLower}):""}}'))
    main.addMassKey(name         = 'Animal or insect',
                    buttonHotkey = keyAnimal,
                    hotkey       = keyAnimal,
                    buttonText   = '', # 'Encounter', # Replace with icon
                    icon         = 'elk-icon.png',
                    target       = '',
                    filter       = f'{{BasicName=={globalCurrentLower}+" a"}}',
                    canDisable   = True,
                    propertyGate = f'{globalNotEncounter}',
                    reportFormat = (f'{{{debug}?("~ Animal or insect for "+'
                                    f'{globalCurrentLower}):""}}'))
    main.addMassKey(name         = 'Personal',
                    buttonHotkey = keyPersonal,
                    hotkey       = keyPersonal,
                    buttonText   = '', # 'Encounter', # Replace with icon
                    icon         = 'per-icon.png',
                    target       = '',
                    filter       = f'{{BasicName=={globalCurrentLower}+" a"}}',
                    canDisable   = True,
                    propertyGate = f'{globalNotEncounter}',
                    reportFormat = (f'{{{debug}?("~ Personal event for "+'
                                    f'{globalCurrentLower}):""}}'))
    main.addMassKey(name         = 'Update',
                    buttonHotkey = keyUpdate,
                    hotkey       = keyUpdate,
                    buttonText   = '', # 'Encounter', # Replace with icon
                    icon         = '',
                    canDisable   = False,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Update phase "+'
                                    f'"Persion="+{globalCurrent}+" "+'
                                    f'"NotMovement="+{globalNotMovement}+" "+'
                                    f'"NotSetup="+{globalNotSetup}+" "+'
                                    f'"NotEncounter="+{globalNotEncounter}+" "'
                                    f'):""}}'))
    main.addMassKey(name         = 'Clean',
                    buttonHotkey = keyClean,
                    hotkey       = keyClean,
                    buttonText   = '', # 'Encounter', # Replace with icon
                    icon         = '',
                    canDisable   = False,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Clean options etc."):""}}'))
    main.addMassKey(name         = 'Increment sleep',
                    buttonHotkey = keyDecrSleep,
                    hotkey       = keyDecrSleep,
                    buttonText   = '', # Replace with icon
                    icon         = '',
                    target       = '',
                    #filter       = f'{{BasicName=={globalCurrentLower}+" S"}}',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    canDisable   = False,
                    singleMap    = False,
                    reportFormat = (f'{{{verbose}?("~ Decrement sleep for "+'
                                    f'{globalCurrentLower}):""}}'))
    
    # ================================================================
    #
    # Some constants
    #
    verb(f'Defining constants')
    firstNames = 'abcd'
    lastNames  = 'acegikmo'
    lvlNames   = 'WLFS'
    prim       = lastNames[0]
    starts     = [['2320','2320'],
                  ['2320','2320'],
                  ['4317','4317'],
                  ['0117','4317'],
                  ['0117','4317'],
                  ['2320','2313']]
    lvls       = {'L': {'rng': range(15,0,-1), 'name': 'Life',
                        'decrLife': {}, 'incrLife': {}},
                  'W': {'rng': range(8,0,-1),  'name': 'Water',
                        'decrLife': {7:1,5:1,4:2,3:4,2:7},
                        'incrLife': {1:7,2:4,3:2,4:1,6:1} },
                  'F': {'rng': range(23,0,-1), 'name': 'Food' ,
                        'decrLife': {20:1,16:1,13:1,10:1,7:1,5:2,4:2,3:2,2:3},
                        'incrLife': {19:1,15:1,12:1, 9:1,6:1,4:2,3:2,2:2,1:3}},
                  'S': {'rng': range(4),    'name': 'Sleep',
                        'decrLife': {},
                        'incrLife': {} },
                  }
    scen    = ['basic',
               'lost',
               'survive',
               'search',
               'rescue',
               'pursue']

    
    # ================================================================
    #
    # Prototypes
    #
    # ----------------------------------------------------------------
    # Clean up prototypes
    verb(f'Processing prototypes')
    prototypes = prototypeContainer.getPrototypes(asdict=False)
    seen       = list()
    for p in prototypes:
        name = p['name']
        verb(f'- prototype: "{name}" {name[0].lower() in firstNames}')
        if name == ' prototype':
            prototypes.remove(p)
        if name in seen:
            # print(f'Removing prototype {p["name"]}')
            prototypes.remove(p)
        seen.append(name)

        if name == 'Markers prototype':
            p.setTraits(p.getTraits().pop());
            
        elif (name[0].lower() in firstNames and name[1:] == ' prototype'):
            traits = p.getTraits()
            basic  = traits.pop()
            delt   = Trait.findTrait(traits,DeleteTrait.ID)
            elim   = Trait.findTrait(traits,SendtoTrait.ID,
                                     key='name',value='Eliminate')
            rept   = Trait.findTrait(traits,ReportTrait.ID)
            if delt: traits.remove(delt)
            if elim: traits.remove(elim)
            if rept: traits.remove(rept)
            traits.append(basic)
            p.setTraits(*traits)
            
            

    prototypes = prototypeContainer.getPrototypes(asdict=True)

    # ----------------------------------------------------------------
    # Make marker prototype
    verb(f'Optional prototypes')
    optProto = prototypes['OptionalMarkers prototype']
    traits   = optProto.getTraits()
    basic    = traits.pop()
    traits   = [TriggerTrait(name       = '',
                             key        = keyToggle,
                             actionKeys = [keySetOpt],
                             property   = f'{{Phase=="{phaseNames[0]}"}}'),
                ClickTrait(key         = keyToggle,
                           context     = False,
                           whole       = True,
                           description = 'Toggle optional rule'),
                ReportTrait(keySetOpt,
                            report = (f'{{{verbose}?("! "+OptName+" "+'
                                      f'(Step_Level==1?"On":"Off")):""}}')),
                basic]
    optProto.setTraits(*traits)
    
    # ----------------------------------------------------------------
    # Make marker prototype
    verb(f'Enable prototypes')
    enbProto = prototypes['EnableMarkers prototype']
    traits   = enbProto.getTraits()
    basic    = traits.pop()
    traits   = [TriggerTrait(name       = '',
                             key        = keyToggle,
                             actionKeys = [keySetOpt],
                             property   = f'{{Phase=="{phaseNames[0]}"}}'),
                ClickTrait(key         = keyToggle,
                           context     = False,
                           whole       = True,
                           description = 'Toggle enable person'),
                ReportTrait(keySetOpt,
                            report = (f'{{{verbose}?("! "+PersonName+" "+'
                                      f'(Step_Level==1?"enabled":"disabled")):'
                                      f'""}}')),
                basic]
    enbProto.setTraits(*traits)

    # ----------------------------------------------------------------
    verb(f'Scenario prototypes')
    scnProto = prototypes['ScenarioMarkers prototype']
    traits   = scnProto.getTraits()
    basic    = traits.pop()
    scnProto.setTraits(basic)

    # ----------------------------------------------------------------
    # Create die roller prototype
    verb(f'Die roll prototype')
    traits = [
        CalculatedTrait(
            name = 'Die',
            expression = f'{{GetProperty("{diceName}_result")}}',
            description = 'Die roll'),
        GlobalHotkeyTrait(
            name         = '',
            key          = keyDieRoll,
            globalHotkey = keyDice,
            description  = 'Roll dice'),
        ReportTrait(keyDice,
                    report=f'{{{verbose}?("Rolled dice {diceName}"):""}}'),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(name        = f'Dice prototype',
                                    description = f'Dice prototype',
                                    traits      = traits)
    # ----------------------------------------------------------------
    # Create random placement prototype
    verb(f'Search prototype')
    colOff = 16
    rowOff = 11
    traits = [
        CalculatedTrait(
             name = 'Col1Die',
             expression = f'{{Integer.parseInt(GetString("{fourDiceName}_result").replaceAll(",[0-9],[0-9],[0-9]",""))}}',
             description = 'Column die roll 1'),
        CalculatedTrait(
             name = 'Col2Die',
             expression = f'{{Integer.parseInt(GetString("{fourDiceName}_result").replaceAll(",[0-9],[0-9]$","").replaceAll("[0-9],",""))}}',
             description = 'Column die roll 2'),
        CalculatedTrait(
             name = 'Row1Die',
             expression = f'{{Integer.parseInt(GetString("{fourDiceName}_result").replaceAll("[0-9],[0-9],","").replaceAll(",[0-9]",""))}}',
             description = 'Row die roll 1'),
        CalculatedTrait(
            name = 'Row2Die',
            expression = f'{{Integer.parseInt(GetString("{fourDiceName}_result").replaceAll("[0-9],[0-9],[0-9],",""))}}',
             description = 'Row die roll 2'),
        CalculatedTrait(
            name = 'Start',
            expression = (f'{{(Col1Die+Col2Die+{colOff}).toString()+'
                          f'"@"+'
                          f'(Row1Die+Row2Die+{rowOff}).toString()}}'),
            description = 'Start hex'),
        MarkTrait(
            name  = 'Type',
            value = 'Search'),
        SendtoTrait(
            mapName     = main['mapName'],
            boardName   = main['mapName'],
            name        = '',
            key         = keyToStart+'Real',
            restoreKey  = '',
            restoreName = '',
            destination = SendtoTrait.GRID,
            position    = '{Start}'),
        GlobalHotkeyTrait(
            name         = '',
            key          = keyFourDice,
            globalHotkey = keyFourDice,
            description  = 'Roll dice'),
        ReportTrait(
            keyFourDice,
            report = (f'{{{debug}?("~ "+'
                      f'" Die roll="+GetString("{fourDiceName}_result")+'
                      f'" Col1Die="+Col1Die+" Col2Die="+Col2Die+'
                      f'" Row1Die="+Row1Die+" Row2Die="+Row2Die'
                      f'):""}}')),
        TriggerTrait(
            name       = f'Start',
            command    = 'To start', #f'{start}',
            key        = keyToStart,
            property   = (f'{{Phase=="Setup"&&'
                          f'{globalScenario}==3||{globalScenario}==4}}'),
            actionKeys = [keyFourDice,key('F'),keyToStart+'Real']),
        ReportTrait(
            keyToStart+'Real',
            report = (f'{{{debug}?("~ "+PieceName+" moved to hex "+Start):""}}')),
        ReportTrait(
            key('F'),
            report = (f'{{{debug}?("~ "+PieceName+" flipped"):""}}')),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(name        = f'Search prototype',
                                    description = f'Search prototype',
                                    traits      = traits)
    
    # ----------------------------------------------------------------
    # Create move ability prototype
    # from numpy import asarray
    dirs = [[  'R 0 F',
               'R 1 F',
               'A 0 F',
               'A 1 F',
               'R 1 F',
               'R 1 F'], # 1
            [  'R 1 F',
               'A 0 F',
               'A 0 F',
               'A 1 F',
               'A 0 F',
               'R 1 F'], # 2 
            [  'R 1 F',
               'A 0 F',
               'A 1 F',
               'A 1 F',
               'A 0 F',
               'A 0 F' ], # 3
            [  'A 0 F',
               'A 0 F',
               'A 1 F',
               'A A A',
               'A 1 A',
               'A 0 F'], # 4
            [  'A 0 F',
               'A 1 F',
               'A 1 F',
               'A A A',
               'A 1 A',
               'A 0 F'], # 5 
            [  'A 0 F',
               'A 1 F',
               'A A A',
               'A A A',
               'A 1 A', 
               'A 1 A'] ] # 6
    # Alternative to numpy transpose from stackoverflow
    dirs = list(zip(*dirs))
    encs = [
        [# 1
            ['',        '',        '',        '',        ''],
            ['',        '',        'F +1',    '',        ''],
            ['F +1',    'F +2',    'F +1',    'F +2',    'F +2'] ],
        [# 2
            ['',         '',           '',           '',           ''], 
            ['',         '',           'W +1',       '',           ''],   
            ['W +1',     'W +1',       'W +1',       'W +1',       'W +1'] ],
        [# 3
            ['S 2',      'S 2',        'S 1',        'S 2',        'S 2'],   
            ['S 1',      'S 1',        '',           'S 1',        'S 1'],   
            ['S 1',      'S 1',        '',           'S 1',        'S 1'] ],
        [# 4
            ['',        '',        '',        '',        ''],
            ['F -1',    'F -1',    'F -1',    'F -1',    'F -1'],
            ['F -2',    'F -2',    'F -1',    'F -2',    'F -2'] ],
        [# 5
            ['S 3',     'S 2',     'S 2',     'S 2',     'S 2'],
            ['',        '',        '',        '',        ''],
            ['W -1',    'W -1',    'W -1',    'W -1',    'W -1'] ],
        [# 6
            ['',        '',        '',        '',        ''],
            ['L -1',    'W -2',    'W -2',    'W -2',    'W -2'],
            ['L -2',    'L -1',    'L -1',    'L -1',    'L -1'] ]
    ]       
    lks = []
    for d, s in zip(dirs, scen[1:5]+['pursued','pursuer']):
        # print(f'Scenario {s:10s}: {",".join(d)}')
        lks.append(':'.join(f'(Die=={n+1})?"{r}"'
                            for n, r in enumerate(d))+':"-"') 
    lk = (f'{{{globalScenario}<2?('+lks[0]+'):'   # Basic+Lost
          f'{globalScenario}==2?('+lks[1]+'):'  # Survive
          f'{globalScenario}==3?('+lks[2]+'):'  # Search
          f'{globalScenario}==4?('+lks[3]+'):'  # Resuce
          f'(PersonName=="A"||PersonName=="C")?('+lks[4]+'):' # Pursued
          f'('+lks[5]+')'
          f'}}') # Pursuer
    ek = f'{{({optEncounter}==false||EncounterEnable==false)?"":'
    for die0, types in enumerate(encs):
        die = die0+1
        tek = f'(Die=={die})?('
        for tpe, scens in enumerate(types):
            ttek = (f'EncounterType=={tpe}?('+
                    ':'.join(f'{globalScenario}=={scen+1}?"{res}"'
                             for scen, res in enumerate(scens)) +
                    ':""):')
            tek += ttek
        tek += '""):'
        ek  += tek
    ek += f'""}}'
    # with open('foo.js','w') as tmp:
    #     print(ek
    #           .replace('(', '\n('),
    #           .replace(')', '\n)'),
    #           file=tmp)
    encCond = (f'{globalCurrent}==PersonName&&'
               f'Phase.contains("Encounter")&&'
               f'{globalScenario}>0&&'
               f'{optEncounter}==true&&')
    calcAb  = key(NONE,0)+',personCalcAbility'
    calcDir = key(NONE,0)+',personCalcDirection'
    calcTrn = key(NONE,0)+',personCalcTurns'
    calcDis = key(NONE,0)+',personCalcDistance'
    traits = [
        PrototypeTrait(name = 'Dice prototype'),
        # Movement abilities
        RestrictCommandsTrait(
            name          = 'Restrict movmemt abilities',
            hideOrDisable = RestrictCommandsTrait.DISABLE,
            expression    = f'{{Phase!=PersonName+" Movement"}}',
            keys          = [keyAbilities]),
        TriggerTrait(
            name       = 'Trigger movement abilities',
            command    = 'Roll movement abilities',
            key        = keyAbilities,
            actionKeys = [keyDieRoll,keyCalcAbility,keyRandomTramp]),
        TriggerTrait(
            name       = 'Trampoline to random direction',
            command    = '',
            key        = keyRandomTramp,
            property   = '{MovementAbilities.startsWith("R")}',
            actionKeys = [keyRandom]),
        TriggerTrait(
            name       = 'Trampoline to random direction',
            command    = '',
            key        = keyCalcAbility,
            actionKeys = [calcAb,calcDir,calcTrn,calcDis]),
        DynamicPropertyTrait(
            ["",calcAb,DynamicPropertyTrait.DIRECT,lk],
            name        = 'MovementAbilities',
            value       = 'R 0 F',
            numeric     = False,
            description = 'Resolve movement capability'),
        DynamicPropertyTrait(
            ["",calcDir,DynamicPropertyTrait.DIRECT,
             '{MovementAbilities.substring(0,1)}'],
            name        = 'MovementDirection',
            value       = 'R',
            numeric     = False,
            description = 'Resolve movement capability'),
        DynamicPropertyTrait(
            ["",calcTrn,DynamicPropertyTrait.DIRECT,
             '{MovementAbilities.substring(2,3)}'],
            name        = 'MovementTurns',
            value       = '0',
            numeric     = False,
            description = 'Number of allowed turns'),
        DynamicPropertyTrait(
            ["",calcDis,DynamicPropertyTrait.DIRECT,
             '{MovementAbilities.substring(2,3)}'],
            name        = 'MovementDistance',
            value       = 'F',
            numeric     = False,
            description = 'How far to move'),
        # CalculatedTrait(
        #    name        = 'MovementDirection',
        #    expression  = '{MovementAbilities.substring(0,1)}',
        #    description = 'Resolve movement capability'),
        # CalculatedTrait(
        #     name        = 'MovementTurns',
        #     expression  = '{MovementAbilities.substring(2,3)}',
        #     description = 'Resolve movement capability'),
        # CalculatedTrait(
        #     name        = 'MovementDistance',
        #     expression  = '{MovementAbilities.substring(4,5)}',
        #     description = 'Resolve movement capability'),
        ReportTrait(
            calcDis,
            report = (f'{{"` "+PersonName+'
                      f'" has movement abilities "+MovementAbilities+" -> "+'
                      f'(MovementDirection=="R"?"random":"any")+" direction, "+'
                      f'(MovementTurns=="A"?"any":MovementTurns.toString())+'
                      f'" turns, "+'
                      f'(MovementDistance=="A"?"any":"full")+" distance "'
                      f'}}')),
        # Random direction
        RestrictCommandsTrait(
            name          = 'Restrict movmemt abilities',
            hideOrDisable = RestrictCommandsTrait.DISABLE,
            expression    = (f'{{Phase!=PersonName+" Movement"}}'),
            keys          = [keyRandom]),
        TriggerTrait(
            name       = 'Trigger random direction',
            command    = 'Roll random direction',
            key        = keyRandom,
            property   = '{MovementDirection=="R"}',
            actionKeys = [keyDieRoll,keyCalcRanDir]),
        ReportTrait(
            keyCalcRanDir,
            report = f'{{"` "+PersonName+" MUST move "+RandomDirection}}'),
        DynamicPropertyTrait(
            ["",keyCalcRanDir,DynamicPropertyTrait.DIRECT,
             ('{Die==2?"NE":Die==3?"SE":Die==4?"S":'
              +'Die==5?"SW":Die==6?"NW":"N"}')],
            name        = "RandomDirection",
            value       = '',
            numeric     = False,
            description = 'Resolve random direction'),
        # Encounter or not
        DynamicPropertyTrait(
            ["",keyEncEnb,DynamicPropertyTrait.DIRECT,'{Die>4}'],
            name = "EncounterEnable",
            numeric = True,
            value   = True,
            description = 'Wheter there is an encounter'),
        # Encounter type 
        ReportTrait(
            keyRndTpe,keyNatTpe,keyAniTpe,keyPerTpe,
            report = (f'{{{debug}?("~ Encounter type is "+'
                      f'(EncounterType==0?"Natural hazard":'
                      f'EncounterType==1?"Animal or insect":'
                      f'EncounterType==2?"Personnal":"Unknown")):""}}')),
        ReportTrait(
            keyEncounter,keyNatural,keyAnimal,keyPersonal,
            report = (f'{{{verbose}?("! Encounter: "+'
                      f'((EncounterStep.toString()!="0")?'
                      f'(EncounterType==0?"Natural hazard":'
                      f'EncounterType==1?"Animal or insect":'
                      f'EncounterType==2?"Personnal":"Unknown")+'
                      f'" = "+EncounterStep:"")+('
                      f'(EncounterEffect=="S")?"<img src=sleep-icon.png>":'
                      f'(EncounterEffect=="W")?"<img src=water-icon.png>":'
                      f'(EncounterEffect=="F")?"<img src=food-icon.png>":'
                      f'(EncounterEffect=="L")?"<img src=life-icon.png>":'
                      f'"None")+" "):""}}')),
                      # f'(EncounterStep+"<img src=sleep-icon.png>")+'
                      # f'(EncounterStep+"<img src=water-icon.png>")+'
                      # f'(EncounterStep+"<img src=food-icon.png>")+'
                      # f'(EncounterStep+"<img src=life-icon.png>")+'
                      # f'"None"))):""}}')),
        ReportTrait(
            keyEncEnb,
            report = (f'{{{debug}?("! "+(EncounterEnable?"Got":"No")+'
                      f'" encounter"):""}}')),
        DynamicPropertyTrait(
            ["",  keyRndTpe,DynamicPropertyTrait.DIRECT,'{Die<4?0:Die<6?1:2}'],
            ["",  keyNatTpe,DynamicPropertyTrait.DIRECT,'{0}'],
            ["",  keyAniTpe,DynamicPropertyTrait.DIRECT,'{1}'],
            ["",  keyPerTpe,DynamicPropertyTrait.DIRECT,'{2}'],
            name =      "EncounterType",
            value       = 0,
            numeric     = True,
            min         = 0,
            max         = 2,
            description = 'Random encounter type'),
        #
        TriggerTrait(
            name       = 'Trigger random encounter type',
            command    = 'Random',
            key        = keyEncounter,
            actionKeys = [keyDieRoll,keyEncEnb,
                          keyDieRoll,keyRndTpe,
                          keyDieRoll,keyEnc],
            property   = f'{{{encCond}{optRandom}==true}}'),
        TriggerTrait(
            name       = 'Trigger natural encounter type',
            command    = 'Natural',
            key        = keyNatural,
            actionKeys = [keyDieRoll,keyEncEnb,keyNatTpe,keyDieRoll,keyEnc],
            property   = f'{{{encCond}{optRandom}==false}}'),
        TriggerTrait(
            name       = 'Trigger animal encounter type',
            command    = 'Animal',
            key        = keyAnimal,
            actionKeys = [keyDieRoll,keyEncEnb,keyAniTpe,keyDieRoll,keyEnc],
            property   = f'{{{encCond}{optRandom}==false}}'),
        TriggerTrait(
            name       = 'Trigger personal encounter type',
            command    = 'Personal',
            key        = keyPersonal,
            actionKeys = [keyDieRoll,keyEncEnb,keyPerTpe,keyDieRoll,keyEnc],
            property   = f'{{{encCond}{optRandom}==false}}'),
        SubMenuTrait(subMenu     = 'Encounter',
                     keys        = ['Random','Natural',
                                    'Animal','Personal'],
                     description = 'Choice of encounters'),
        RestrictCommandsTrait(
            name          = 'Restrict encounter choice',
            hideOrDisable = RestrictCommandsTrait.HIDE,
            expression    = f'{{{optRandom}==true}}',
            keys          = [keyNatTpe,keyAniTpe,keyPerTpe]),
        RestrictCommandsTrait(
            name          = 'Restrict encounter random',
            hideOrDisable = RestrictCommandsTrait.HIDE,
            expression    = f'{{{optRandom}==false}}',
            keys          = [keyRndTpe]),
        # Encounters
        CalculatedTrait(
            name        = 'Encounter',
            expression  = ek,
            description = 'Resolve encounter'),
        CalculatedTrait(
            name          = 'EncounterEffect',
            expression    = ('{Encounter.length()>0?'
                             'Encounter.substring(0,1):""}'),
            description   = 'The encounter effect'),
        CalculatedTrait(
            name          = 'EncounterStep',
            expression    = ('{Encounter.length()>0?'
                             'Integer.parseInt(Encounter.substring(2)):0}'),
            description   = 'Step of encounter effect'),
        CalculatedTrait(
            name          = 'EncounterAStep',
            expression    = ('{Math.abs(EncounterStep)}'),
            description   = 'Step of encounter effect'),
        # Dollar signs since personName and EncounterEffect are
        # properties of sending piece - not reciever candidate pieces
        GlobalCommandTrait(
            commandName = '',
            key         = keyIncrReal+'Trampoline',
            globalKey   = keyIncr,
            properties  = f'{{BasicName=="$personName$ $EncounterEffect$"}}',
            description = f'Increment for person'),       
        GlobalCommandTrait(
            commandName = '',
            key         = keyDecrReal+'Trampoline',
            globalKey   = keyDecr,
            properties  = f'{{BasicName=="$personName$ $EncounterEffect$"}}',
            description = f'Decrement for person'),
        TriggerTrait(
            name        = 'Increment effect',
            command     = '',
            key         = keyIncrReal+'Start',
            actionKeys  = [keyIncrReal+'Trampoline'],
            loop        = True,
	    count       = '{EncounterStep}',
            property    = '{EncounterStep>0}'),
        TriggerTrait(
            name        = 'Decrement effect',
            command     = '',
            key         = keyDecrReal+'Start',
            actionKeys  = [keyDecrReal+'Trampoline'],
            loop        = True,
	    count       = '{EncounterAStep}',
            property    = '{EncounterStep<0}'),
        ReportTrait(
            keyIncrReal+'Start',
            report = (f'{{{debug}?("~ Encounter increment effect=("+'
                      f'EncounterEffect+") step=("+'
                      f'EncounterStep+")"):""}}')),
        ReportTrait(
            keyDecrReal+'Start',
            report = (f'{{{debug}?("~ Encounter decrement effect=("+'
                      f'EncounterEffect+") step=("+'
                      f'EncounterAStep+")"):""}}')),
        ReportTrait(
            keyIncrReal+'Trampoline',
            report = (f'{{{debug}?("~ Encounter increment trampoline effect=("+'
                      f'EncounterEffect+") step=("+'
                      f'EncounterStep+")"):""}}')),
        ReportTrait(
            keyDecrReal+'Trampoline',
            report = (f'{{{debug}?("~ Encounter decrement trampoline effect=("+'
                      f'EncounterEffect+") step=("+'
                      f'EncounterAStep+")"):""}}')),
        ReportTrait(
            keyEnc,
            report = (f'{{{debug}?("~ Encounter for ~"+'
                      f'personName+"~ is ~"+'
                      f'Encounter+"~ -> ~"+'
                      f'EncounterEffect+"~ with ~"+'
                      f'EncounterStep+"~ ("+'
                      f'EncounterAStep+")"):""}}')),
        TriggerTrait(
            name       = 'Trigger encounter',
            command    = '',
            key        = keyEnc,
            actionKeys = [keyIncrReal+'Start',keyDecrReal+'Start']),
        BasicTrait()
    ]
          
    prototypeContainer.addPrototype(
        name        = f'Person prototype',
        traits      = traits,
        description = f'Prototype for persons')
    
        
    
    
    
    # ----------------------------------------------------------------
    # Make marker prototype
    verb(f'Marker prototypes')
    for ln, lp in lvls.items():
        traits     = []
        incrTrig   = []
        decrTrig   = []
        incrLife   = []
        decrLife   = []
        mrepKeys    = []
        lrepKeys    = []
        rng        = list(lp['rng'])
        var        = lp['name']
        dyn        = DynamicPropertyTrait(
            ['',keyIncrVal,ChangePropertyTrait.DIRECT,
             fr'{{Math.min(Value+1,{max(rng)})}}'],
            ['',keyDecrVal,ChangePropertyTrait.DIRECT,
             fr'{{Math.max(Value-1,{min(rng)})}}'],
            name    = 'Value',
            value   = rng[0],
            numeric = True,
            min     = min(rng),
            max     = max(rng))
        traits.append(dyn)
        # print(ln,var,rng)
        
        verb(f' {var} Marker prototype')
        # traits.extend([GlobalHotkeyTrait(name         = '',
        #                                  key          = keyIncrLife,
        #                                  globalHotkey = keyIncrLife,
        #                                  description  = 'Increment life'),
        #                GlobalHotkeyTrait(name         = '',
        #                                  key          = keyDecrLife,
        #                                  globalHotkey = keyDecrLife,
        #                                  description  = 'Decrement life')])
        traits.extend([
            GlobalCommandTrait(
                commandName  = '',
                key          = keyIncrLife,
                globalKey    = keyIncr,
                properties   = '{BasicName=="$personName$ L"}',
                description  = 'Increment life'),
            GlobalCommandTrait(
                commandName  = '',
                key          = keyDecrLife,
                globalKey    = keyDecr,
                properties   = '{BasicName=="$personName$ L"}',                
                description  = 'Decrement life')])

        # Increment and decrement _must_ trampoline, otherwise other
        # triggers with same name may be called which will move the
        # maker to the top or bottom, depending on the order of
        # trigger traits
        ikeys = [keyIncrReal,keyIncrVal]
        dkeys = [keyDecrReal,keyDecrVal]
        if ln == 'L':
            ikeys.append(keySetLife)
            dkeys.append(keySetLife)
        if ln == 'S':
            incrTrig.append(
                TriggerTrait(name       = f'Decrement {var} explicit',
                             command    = '',
                             key        = keyDecrSleep,
                             actionKeys = dkeys,
                             property   = '{{Phase==PersonName+" Movement"}}'))
        incrTrig.append(
            TriggerTrait(name       = f'Increment {var}',
                         command    = 'Increment',
                         key        = keyIncr,
                         actionKeys = ikeys))
        decrTrig.append(
            TriggerTrait(name       = f'Decrement {var}',
                         command    = 'Decrement',
                         key        = keyDecr,
                         actionKeys = dkeys))
        for ll in rng:
            if ll != max(rng):
                mk    = keyMove+f'{ll+1}'
                keys  = [mk]
                mrepKeys.append(mk)
                if ln == 'L': keys.extend([keySet])
                
                incrL = lp['incrLife'].get(ll,0)
                if incrL > 0:
                    # send global key to OOB, which goes to life marker
                    lk = keyIncrLife+f'{ll}'
                    incrLife.append(
                        TriggerTrait(name  = f'Increment life {incrL} times',
                                     command    = '',
                                     key        = lk,
                                     actionKeys = [keyIncrLife],
                                     loop       = True,
                                     count      = incrL))
                    keys.append(lk)
                    lrepKeys.append(lk)
                                     
                incrTrig.append(
                    TriggerTrait(name       = f'Increment {var}',
                                 command    = '',
                                 key        = keyIncrReal,
                                 actionKeys = keys,
                                 property   = (f'{{Value=={ll}}}')))
            if ll != min(rng):
                mk    = keyMove+f'{ll-1}'
                keys  = [mk]
                mrepKeys.append(mk)
                
                if ln == 'L': keys.extend([keySet])
                
                decrL = lp['decrLife'].get(ll,0)
                if decrL > 0:
                    # send global key to OOB, which goes to life marker
                    lk = keyDecrLife+f'{ll}'
                    decrLife.append(
                        TriggerTrait(name  = f'Decrement life {decrL} times',
                                     command    = '',
                                     key        = lk,
                                     actionKeys = [keyDecrLife],
                                     loop       = True,
                                     count      = decrL))
                    keys.append(lk)
                    lrepKeys.append(lk)

                decrTrig.append(
                    TriggerTrait(name       = f'Decrement {var}',
                                 command    = '',
                                 key        = keyDecrReal,
                                 actionKeys = keys,
                                 property   = (f'{{Value=={ll}}}')))
        traits.append(RestrictCommandsTrait(
            name = 'Restrict increment and decrement',
            hideOrDisable = RestrictCommandsTrait.DISABLE,
            expression    =  ('{Phase.contains(PersonName)==false||('
                              'Phase.contains("Necessities")==false&&'
                              'Phase.contains("Encounter")==false)}'),
            keys          = [keyIncr,keyDecr]))
        traits.extend(incrTrig)
        traits.extend(decrTrig)
        traits.extend(incrLife)
        traits.extend(decrLife)
        traits.append(ReportTrait(keyIncrVal,keyDecrVal,
                                  report = (f'{{{debug}?("~ "+'
                                            f'BasicName+" incr or decr value="'
                                            f'+Value):'
                                            f'""}}')))
        traits.append(ReportTrait(*mrepKeys,
                                  report = (f'{{{debug}?("~ "+'
                                            f'BasicName+" move value="+Value):'
                                            f'""}}')))
        traits.append(ReportTrait(*lrepKeys,
                                  report = (f'{{{debug}?("~ "+'
                                            f'BasicName+" set life="+Value):'
                                            f'""}}')))
        if ln == 'L':
            traits.append(
                ReportTrait(keySet,
                            report = (f'{{{debug}?("~ "+'
                                      f'BasicName+" set value of life="'
                                      f'+Value):""}}')))                
        if ln == 'S':
            traits.append(
                ReportTrait(keyDecrSleep,
                            report = (f'{{{debug}?("~ "+'
                                      f'BasicName+'
                                      f'" auto decrement sleep value="'
                                      f'+Value):""}}')))                

        traits.append(BasicTrait())
        prototypeContainer.addPrototype(
            name        = f'{var} prototype',
            traits      = traits,
            description = f'Prototype for {var} markers')
    
    # ================================================================
    #
    # Get _all_ pieces
    #
    verb(f'Processing all pieces')
    allPieces  = game.getPieces(False)
    for piece in allPieces:
        name     = piece['entryName']
        par      = piece.getParent(None,False)
        startReg = None
        startOOB = None

        # Add hidden unit 
        if name == hidden:
            traits = piece.getTraits()
            basic  = traits.pop()
            keySetNotMovement  = key(NONE,0)+',hiddenNotMovement'
            keySetNotSetup     = key(NONE,0)+',hiddenNotSetup'
            keySetNotEncounter = key(NONE,0)+',hiddenNotEncounter'
            keySetCurrent      = key(NONE,0)+',hiddenCurrent'
            keySetCurrentLower = key(NONE,0)+',hiddenCurrentLower'
            keyCleanEncounter  = key(NONE,0)+',hiddenCleanEncounter'
            keyCleanRandom     = key(NONE,0)+',hiddenCleanRandom'
            traits.extend([
                GlobalCommandTrait(
                    commandName = '',
                    key         = keyDecrSleep,
                    globalKey   = keyDecrSleep,
                    properties  = (f'{{BasicName=={globalCurrentLower}+" S"&&'
                                   f'Phase=={globalCurrent}+" Movement"}}'),
                    description = f'Decrement sleep for person'),       
                GlobalHotkeyTrait(
                    name         = '',
                    key          = keySetup,
                    globalHotkey = keySetup,
                    description  = 'Show set-up window'),
                GlobalPropertyTrait(
                    ["",keySetCurrent,GlobalPropertyTrait.DIRECT,
                     '{Phase.substring(0,1)}'],
                    name    = globalCurrent,
                    numeric = True),
                GlobalPropertyTrait(
                    ["",keySetCurrentLower,GlobalPropertyTrait.DIRECT,
                     '{Phase.substring(0,1).toLowerCase()}'],
                    name    = globalCurrentLower,
                    numeric = True),
                GlobalPropertyTrait(
                    ["",keySetNotMovement,GlobalPropertyTrait.DIRECT,
                     '{!Phase.contains("Movement")?true:false}'],
                    name    = globalNotMovement,
                    numeric = True),
                GlobalPropertyTrait(
                    ["",keySetNotSetup,GlobalPropertyTrait.DIRECT,
                     ('{(!Phase.contains("Setup")&&!Phase.contains("Start"))'
                      '?true:false}')],
                    name    = globalNotSetup,
                    numeric = True),
                GlobalPropertyTrait(
                    ["",keySetNotSetup,GlobalPropertyTrait.DIRECT,
                     f'{{(!Phase.contains("Encounter")||{optRandom})?true:false}}'],
                    name    = globalNotEncounter,
                    numeric = True),
                GlobalPropertyTrait(
                    ["",keyCleanEncounter,GlobalPropertyTrait.DIRECT,
                     f'{{{globalScenario}==0?false:{optEncounter}}}'],
                    name    = optEncounter,
                    numeric = True),
                GlobalPropertyTrait(
                    ["",keyCleanRandom,GlobalPropertyTrait.DIRECT,
                     f'{{{optEncounter}==false?false:{optRandom}}}'],
                    name    = optRandom,
                    numeric = True),
                TriggerTrait(
                    name       = 'Update phase properties',
                    command    = '',
                    key        = keyUpdate,
                    actionKeys = [keySetCurrent,
                                  keySetCurrentLower,
                                  keySetNotMovement,
                                  keySetNotSetup,
                                  keySetNotEncounter,
                                  keyDecrSleep]),
                TriggerTrait(
                    name       = 'Clean up options',
                    command    = '',
                    key        = keyClean,
                    actionKeys = [keyCleanEncounter,keyCleanRandom]),
                ReportTrait(
                    keyUpdate,
                    report = (f'{{{debug}?("` Update phase ~"+Phase+"~ "+'
                              f'"Persion=~"+{globalCurrent}+"~ "+'
                              f'"NotMovement="+{globalNotMovement}+" "+'
                              f'"NotSetup="+{globalNotSetup}+" "+'
                              f'"NotEncounter="+{globalNotEncounter}+" "'
                              f'):""}}')),                    
                ReportTrait(
                    keyDecrSleep,
                    report = (f'{{{debug}?("` Possible decrement sleep ~"+'
                              f'Phase+"~ Persion=~"+{globalCurrent}+"~ "'
                              f'):""}}')),                    
                basic
            ])
            piece.setTraits(*traits)
            
            main.addAtStart(name            = name,
                            location        = '',
                            useGridLocation = False,
                            owningBoard     = main['mapName'],
                            x               = 0,
                            y               = 0).addPiece(piece)
            continue

        if name.startswith('opt'):
            traits  = piece.getTraits()
            basic   = traits.pop()
            optName = name.replace('opt','').replace(' ','')
            gpName  = name.replace(' ','')
            # print(f'Option marker {name} -> "{optName}" "{gpName}"')
            
            step    = Trait.findTrait(traits,LayerTrait.ID,
                                     key = 'name', value = 'Step')
            step["newNames"]     = "+ enabled,+ disabled"
            step["increaseName"] = ''
            step["decreaseName"] = ''
            step["increaseKey"]  = ''
            step["decreaseKey"]  = ''
            step["loop"]         = False
            step['follow']       = True
            step["expression"]   = f'{{{gpName}==true?1:2}}'
            traits.extend([
                MarkTrait(name="OptName",value=optName),
                GlobalPropertyTrait(
                    ["",keySetOpt,GlobalPropertyTrait.DIRECT,
                     f'{{!{gpName}}}'],
                    name = gpName,
                    numeric = True,
                    description = f'Toggle {gpName}'),
                ReportTrait(keySetOpt,
                            report = (f'{{{debug}?('
                                      f'"~ Option {optName} set to "+'
                                      f'{gpName}):""}}')),
                basic])
            piece.setTraits(*traits)
            continue 

        if name.endswith('enable'):
            traits  = piece.getTraits()
            basic   = traits.pop()
            perName = name.replace('enable','').replace(' ','')
            gpName  = perName.upper()+'_Enable'
            
            step    = Trait.findTrait(traits,LayerTrait.ID,
                                     key = 'name', value = 'Step')
            step["newNames"]     = "+ enabled,+ disabled"
            step["increaseName"] = ''
            step["decreaseName"] = ''
            step["increaseKey"]  = ''
            step["decreaseKey"]  = ''
            step["loop"]         = False
            step['follow']       = True
            step["expression"]   = f'{{{gpName}==true?1:2}}'
            traits.extend([
                MarkTrait(name="PersonName",value=perName.upper()),
                MarkTrait(name="personName",value=perName.lower()),
                step,
                GlobalPropertyTrait(
                    ["",keySetOpt,GlobalPropertyTrait.DIRECT,
                     f'{{!{gpName}}}'],
                    name    = gpName,
                    numeric = True,
                    description = f'Toggle {gpName}'),
                ReportTrait(keySetOpt,
                            report = (f'{{{debug}?('
                                      f'"~ Person {perName} set to "+'
                                      f'{gpName}):""}}')),
                basic])
            piece.setTraits(*traits)
            continue 

        if name == 'scenario basic':
            traits  = piece.getTraits()
            basic   = traits.pop()
            imgs    = [f'scenario_{s}.png' for s in scen ]
            step    = LayerTrait(imgs, 
                                 ','.join(scen),
                                 activateName = '',
                                 increaseName = '',
                                 decreaseName = '',
                                 increaseKey  = '',
                                 decreaseKey  = '',
                                 name         = 'Scenario',
                                 follow       = True,
                                 expression   = f'{{{globalScenario}+1}}')
            click = ClickTrait(key         = '',
                               context     = True,
                               whole       = True,
                               description = 'Open scenario menu')
            keys  = [key(NONE,0)+f',set{s}' for s in scen]
            globs = [
                GlobalPropertyTrait(
                    [s.capitalize(),key,
                     GlobalPropertyTrait.DIRECT,
                     f'{{{no}}}'],
                    name = globalScenario,
                    numeric = True,
                    description = f'Set scenario')
                for no, (s,key) in enumerate(zip(scen,keys))]
            reps = [
                ReportTrait(key,
                            report = (f'{{{verbose}?("! Scenario set to '
                                      f'{s.capitalize()} ("+'
                                      f'{globalScenario}+")"):""}}'))
                for no, (s,key) in enumerate(zip(scen,keys))]
            res = RestrictCommandsTrait(
                name          = 'Restrict scenario choice',
                hideOrDisable = RestrictCommandsTrait.DISABLE,
                expression    = f'{{Phase!="Start"}}',
                keys          = keys)
            traits.extend([step,click,res] + globs + reps + [basic])
            piece.setTraits(*traits)
            continue

        # Lost persons and decoys
        if name.startswith('decoy') or name.startswith('mia'):
            traits  = piece.getTraits()
            basic   = traits.pop()
            mark    = Trait.findTrait(traits,PrototypeTrait.ID,
                                     key = 'name', value = 'Markers prototype')
            rept    = Trait.findTrait(traits,ReportTrait.ID)
            if mark: traits.remove(mark)
            if rept: traits.remove(rept)
            
            step    = Trait.findTrait(traits,LayerTrait.ID,
                                     key = 'name', value = 'Step')
            step["newNames"]     = "+,Unknown"

            traits.append(PrototypeTrait(name='Search prototype'))
            traits.append(basic)
            piece.setTraits(*traits)
            continue;
        
        # Remove spurious counters
        if name[0] in firstNames and \
           name[1] == ' ' and \
           name[2] in lastNames:
            if par and isinstance(par,AtStart):
                gpar = par.getParent(None,False)
                if gpar and gpar['mapName'] != 'Setup':
                    gpar.remove(par)
                    continue
                
            if par and isinstance(par,ListWidget) and name[2] in lastNames[1:]:
                par.remove(piece)
                continue

        print(name)
        # Process player counter and marks 
        if name[0] in firstNames and \
           name[1] == ' ' and \
           (name[2] == prim or name[2] in lvlNames):
            first  = name[0]
            First  = first.upper()
            last   = name[2]
            traits = piece.getTraits();
            basic  = traits.pop()
            step   = Trait.findTrait(traits,LayerTrait.ID,
                                     key='name',value='Step')
            
            traits.append(MarkTrait(name="PersonName", value=First))
            traits.append(MarkTrait(name="personName", value=first))
            
            # Process the person counter 
            if last == prim:

                gp.addProperty(name         = f'{First}_Enable',
                               initialValue = True,
                               isNumeric    = True,
                               min          = 0,
                               max          = 1)
                gp.addProperty(name         = f'{First}_Life',
                               initialValue = list(lvls['L']['rng'])[-1],
                               isNumeric    = True,
                               min          = 0,
                               max          = 1)

                strt = step['images'].split(',')
                imgs = []
                nms  = []
                for l in lastNames:
                    imgs.extend([i.replace(f'_{prim}',f'_{l}') for i in strt])
                    nms .append(f'+ life={l.upper()}')
                    nms .append(f'+ life={chr(ord(l)+1).upper()}')

                step['images']        = ','.join(imgs)
                step['newNames']      = ','.join(nms)
                step['increaseName']  = ''
                step['decreaseName']  = ''
                step['increaseKey']   = '' # keyIncr
                step['decreaseKey']   = '' # keyDecr
                step['follow']        = True
                step['expression']    = f'{{{First}_Life}}'

                trig = []
                send = []
                rep  = []
                for i, start in enumerate(starts):
                    # print(name,start)
                    j         = i
                    realKey   = keyToStart+f'{j}'
                    realStart = start[0] if First in 'AC' else start[1]
                    realStart = realStart[:2]+'@'+realStart[2:]
                    trig.append(
                        TriggerTrait(name       = f'Start for scenario {j}',
                                     command    = '', #f'{start}',
                                     key        = keyToStart,
                                     property   = (f'{{{First}_Enable==true&&'
                                                   f'{globalScenario}=={j}}}'),
                                     actionKeys = [realKey]))
                    send.append(
                        SendtoTrait(mapName     = main['mapName'],
                                    boardName   = main['mapName'],
                                    name        = '',
                                    key         = realKey,
                                    restoreKey  = '',
                                    restoreName = '',
                                    destination = SendtoTrait.GRID,
                                    position    = realStart))
                    rep.append(
                        ReportTrait(realKey,
                                    report = (f'{{{verbose}?("! "+'
                                              f'"{First} start in hex "'
                                              f'+"{realStart}"):""}}')))
                        

                traits.append(
                    ReportTrait(keyToStart,
                                report = (f'{{{debug}?("~ "+PersonName'
                                          f'+" to start hex"):""}}')))
                traits.append(MarkTrait(name="Type",value="Person"))
                traits.append(PrototypeTrait(name="Person prototype"))
                traits.extend(rep)
                traits.extend(send)
                traits.extend(trig)

            # Process markers 
            else:
                fac        = Trait.findTrait(traits,PrototypeTrait.ID,
                                             key='name',
                                             value=f'{First} prototype');
                rep        = Trait.findTrait(traits,ReportTrait.ID)
                # print(f'{name}:',traits)
                if step: traits.remove(step)
                if fac:  traits.remove(fac)
                if rep:  traits.remove(rep)

                oob        = f'{First} levels'
                rng        = lvls[last]['rng']
                var        = lvls[last]['name']
                zone       = f'{first} {var.lower()} levels'

                send       = []
                rep        = []
                start      = None
                
                if last == 'L':
                    traits.extend([
                        GlobalPropertyTrait(
                            ['',keySetLife,ChangePropertyTrait.DIRECT,
                             f'{{{rng[0]}-Value+1}}'],
                            name    = f'{First}_Life',
                            numeric = True,
                            min     = rng[0],
                            max     = rng[1],
                            level   = ''),
                        ReportTrait(
                            keySetLife,
                            report = (f'{{{verbose}?('
                                      f'"! {First} <img src=life-icon.png> now "+'
                                      f'{First}_Life):""}}'))])

                for ll in rng:
                    # print(f'{First} - {ll}')
                    if last == 'L':
                        reg = f'{first} {chr(ord("o")+1-ll)}@{zone}'
                    else:
                        reg = f'{first} {var.lower()} level {ll}@{zone}'
                    if start is None: start = reg
                        
                    send.append(
                        SendtoTrait(mapName     = oob,
                                    boardName   = oob,
                                    key         = keyMove+f'{ll}',
                                    restoreName = '',
                                    restoreKey  = '',
                                    destination = SendtoTrait.REGION,
                                    zone        = zone,
                                    region      = reg))
                    rep.append(
                        ReportTrait(keyMove+f'{ll}',
                                    report = (f'{{{debug}?("~ "+'
                                            f'BasicName+" move to="'
                                            f'+"{reg}"):""}}')))

                traits.extend(send)
                traits.extend(rep)
                traits.append(PrototypeTrait(name=f'{var} prototype'))
                traits.append(RestrictAccessTrait(sides=[],
                                                  description='Cannot move'))
                startOOB = maps[f'{First} levels']
                startReg = start
                                                        
            traits.append(basic)
            piece.setTraits(*traits)

            if startOOB and startReg:
                # print(f'Set start of {name} to {startOOB["mapname"]}'
                #       f' at {startReg}')
                startOOB.addAtStart(name     = name,
                                    location = startReg).addPiece(piece)
                
                    
#
# EOF
#

                      
    
    
